#!/usr/bin/env python3
"""
Script care genereaza secventa pentru luminat becuri.

Acest script genereaza un fisier care contine OPCODES prin intermediul carora
se modifica starea ledurilor pe instalatia de pe brad. Fisierul este transmis
catre brad prin intermediul interfetei web. Fisierul general o sa fie arhivat
pentru a ocupa mai putin spatiu.

Pe interfata web fiecare user poate urca maxim 3 secvente. Secventele sunt apoi
rulate in mod aleator. Exista si posibilitatea testarii unei secvente inainte 
de a fi urcata pe server. Testarea este limitata la 40 de secunde, iar
secventele principale sunt limitate la 180 de secunde.

README:
    Clasa NeoPixel returneaza un array de pixeli.
    Culorile sunt exprimate ca tuple RGB cu valori cuprinse intre 0 si 255.
    Functii disponibile:
    - fill((r, g, b))
        seteaza toti pixelii la culoarea RGB data
        functia fill face si commit
    - sleep(milliseconds)
        destul de descriptiv
    - commit()
        trimite starea actuala a pixelilor catre becuri
    - push()
        dupa incheierea secventei se apeleaza functia push pentru a scrie
    datele in fisierul out

Pixelii pot fi adresati si individual:
    pixels[2] = (r, g, b)
Aceasta modifica starea lor in array. Pentru a trimite modificarile catre bec
trebuie sa se faca commit.

Pentru debugging foloseste 'decoder.py'
"""

import neopixel
import random
import os
import sys

PIXEL_NUM = 100

path = os.path.join(os.getcwd(), 'animation.txt')
if len(sys.argv) > 1:
    path = sys.argv[1]

pixels = neopixel.NeoPixel(PIXEL_NUM, path)

def wheel(pos):
    # Input a value 0 to 255 to get a color value.
    # The colours are a transition r - g - b - back to r.
    if pos < 0 or pos > 255:
        r = g = b = 0
    elif pos < 85:
        r = int(pos * 3)
        g = int(255 - pos*3)
        b = 0
    elif pos < 170:
        pos -= 85
        r = int(255 - pos*3)
        g = 0
        b = int(pos*3)
    else:
        pos -= 170
        r = 0
        g = int(pos*3)
        b = int(255 - pos*3)
    return (r, g, b)

def rainbow_cycle(wait):
    for j in range(255):
        for i in range(50):
            pixel_index = (i * 256 // 50) + j
            pixels[i] = wheel(pixel_index & 255)
        pixels.commit()
        pixels.sleep(wait)


# Exemplu animatie
# Inlocuieste codul din try-except cu propiul tau algoritm
try:
    while True:
        pixels.fill((0,0,0))
        pixels.sleep(30)
        pixels.fill((255,255,255))
        pixels.sleep(30)

except KeyboardInterrupt:
    pixels.fill((0, 0, 0))
    pixels.commit()

# La final se face push pentru a se arhiva datele si a se scrie in fisierul out
pixels.push()